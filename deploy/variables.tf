variable "prefix" {
  type        = string
  default     = "raad"
  description = "recipe app api devops"
}

variable "project" {
  type        = string
  default     = "recipe app api devops"
  description = "project name"
}

variable contact {
  type        = string
  default     = "yuezhang2010@hotmail.com"
  description = "email address"
}
